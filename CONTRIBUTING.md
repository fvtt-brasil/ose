Português ([english version](#english "English version"))
=========================================================

## Contribuindo
Todas as contribuições são bem vindas, se você quer ajudar com a tradução dos termos basta acessar o projeto no [Crowdin](https://fvtt.crowdin.com/ose), criar uma conta e começar a traduzir, a conta é gratuita, não existe qualquer custo envolvido para ajudar com a tradução.

Se você encontrar algum erro na tradução você pode fazer a correção da mesma no projeto no [Crowdin](https://fvtt.crowdin.com/core) ou então criar uma nova [*issue*](https://gitlab.com/fvtt-brasil/ose/-/issues) aqui mesmo, **não crie** *merge requests* com alterações na tradução, elas não serão aceitas.

Se você quer ajudar com o código do módulo fique atendo aos seguintes requisitos:

- Esse repositório segue o [Conventional Commits](https://www.conventionalcommits.org/), todos os *commits* devem estar de acordo com essa especificação.

- Esse repositório também segue o estilo [Semi-Standard](https://standardjs.com), todos os *commits* passam automaticamente por uma validação, *merge requests* que falhem nesse teste não serão aceitas.

- As *tags* e *releases* são geradas automaticamente através do [semantic-release](https://github.com/semantic-release/semantic-release), que define a versão a ser lançada de acordo com a mensagem dos *commits*, não é necessário atualizar o número da versão em nenhum arquivo, mantenha eles como estão, tudo isso sera atualizado de forma automática.
<br/>

English
=======

## Contributing
All contributions are welcome, if you want to help with the translation of the terms just access the project on [Crowdin](https://fvtt.crowdin.com/ose), create an account and start translating, the account is free, there is no cost involved to help with the translation.

If you find any error in the translation you can correct it in the project on [Crowdin](https://fvtt.crowdin.com/core) or create a new [*issue*](https://gitlab.com/fvtt-brasil/ose/-/issues) right here, **do not create** *merge requests* with changes in the translation, they will not be accepted.

If you want to help with the module code, please make sure you meet the following requirements:

- This repository follows the [Conventional Commits](https://www.conventionalcommits.org/), all *commits* must comply with this specification.

- This repository also follows the [Semi-Standard](https://standardjs.com) style, all *commits* automatically go through a validation, *merge requests* that fail this test will not be accepted.

- The *tags* and *releases* are automatically generated through the [semantic-release](https://github.com/semantic-release/semantic-release), which defines the version to be released according to the message of the *commits*, it is not necessary to update the version number in any file, keep them as they are, all this will be updated automatically.
