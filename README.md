[![Version](https://img.shields.io/badge/dynamic/json?color=blue&style=flat-square&label=Version&prefix=v&query=version&url=https%3A%2F%2Fgitlab.com%2Ffvtt-brasil%2Fose%2F-%2Fraw%2Fmain%2Fose-ptbr%2Fmodule.json)](https://gitlab.com/fvtt-brasil/ose) [![Compatible](https://img.shields.io/badge/dynamic/json?color=orange&style=flat-square&label=Compatible&prefix=v&query=compatibleCoreVersion&url=https%3A%2F%2Fgitlab.com%2Ffvtt-brasil%2Fose%2F-%2Fraw%2Fmain%2Fose-ptbr%2Fmodule.json)](http://foundryvtt.com/) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow?style=flat-square)](https://opensource.org/licenses/MIT) [![Discord invite](https://img.shields.io/badge/Chat-on_Discord-blue?logo=discord&style=flat-square&logoColor=white)](https://discord.gg/XNC86FBnQ2) [![pt-BR translation](https://img.shields.io/badge/dynamic/json?color=blue&label=pt-BR&style=flat-square&query=%24.progress.0.data.translationProgress&url=https%3A%2F%2Fbadges.awesome-crowdin.com%2Fstats-200008372-13.json)](https://fvtt.crowdin.com/ose)



Foundry VTT Old School Essentials Brazilian Portuguese
======================================================

## Português ([english version](#english "English version"))

Esse módulo adiciona o idioma Português (Brasil) como uma opção a ser selecionada nas configurações do FoundryVTT. Selecionar essa opção traduzirá a ficha de personagem do sistema OSE e seus compêndios.
Esse módulo traduz somente aspectos relacionados ao sistema de [Old School Essentials](https://gitlab.com/mesfoliesludiques/foundryvtt-ose). Esse módulo não traduz outras partes do software Foundry VTT, como a interface principal. Para isso, confira o módulo [Brazillian Portuguese Core](https://foundryvtt.com/packages/ptBR-core/).

Para a tradução dos Compêndios, é necessário que seja instalado o módulo [Babele](https://foundryvtt.com/packages/babele/).

Se você quer ajudar nesse projeto, seja com contribuições para o código ou tradução, por favor leia o nosso [guia de contribuições](CONTRIBUTING.md), toda ajuda é bem vinda.


### Instalação
A tradução está disponível na lista de Módulos Complementares para instalar com o nome de Translation: Brazilian Portuguese [OSE].

Lembre-se de também instalar o módulo [Babele](https://foundryvtt.com/packages/babele/) para a tradução dos Compêndios.

#### Instalação por Manifesto

Na opção `Add-On Modules` clique em `Install Module` e coloque o seguinte link no campo `Manifest URL`

`https://gitlab.com/fvtt-brasil/ose/-/raw/main/ose-ptbr/module.json`

#### Instalação Manual (Não recomendado)

Se as opções acima não funcionarem, visite a página com os [pacotes](https://gitlab.com/fvtt-brasil/core/-/packages), clique na versão que você deseja usar, faça o download do arquivo `ose-ptbr.zip` e extraia o conteúdo na pasta `Data/modules/`.

Feito isso ative o módulo nas configurações do mundo em que pretende usá-lo e depois altere o idioma nas configurações.

<br/>

## English
This module adds the language Português (Brasil) as an option to be selected in the Foundry VTT settings. Selecting this option will translate the OSE system character sheet and its compendiums.
This module translates only system-related aspects of [Old School Essentials](https://gitlab.com/mesfoliesludiques/foundryvtt-ose). This module does not translate other parts of the Foundry VTT software, such as the main interface. For that, check out the module [Brazillian Portuguese Core](https://foundryvtt.com/packages/ptBR-core/).

For translating the Compendium packs, it is necessary to also install the [Babele](https://foundryvtt.com/packages/babele/) module.

If you want to help with this project, either with contributions to the code or translation, please read our [contributions guide](CONTRIBUTING.md), any help is welcome.


### Installation
The translation is available in the list of Add-on Modules to install under the name Translation: Brazilian Portuguese [OSE].

Remember to also install the [Babele](https://foundryvtt.com/packages/babele/) module for Compendium pack translation.

#### Installation by Manifest

In the option `Add-On Modules` click on `Install Module` and place the following link in the field `Manifest URL`

`https://gitlab.com/fvtt-brasil/ose/-/raw/main/ose-ptbr/module.json`

### Manual Installation
If the above options do not work, visit the page with the [packages](https://gitlab.com/fvtt-brasil/core/-/packages), click on the version you want to use, download the `ose-ptbr.zip` file and extract the contents into the `Data/modules/` folder.

Once this is done, activate the module in the settings of the world in which you want it and then change the language in the settings.
